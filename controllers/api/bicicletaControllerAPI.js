var Bicicleta = require('../../models/bicicleta');

// const {NotExtended} = require('http-errors');

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function (req, res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);

    Bicicleta.add(bici);

    // res.status(200).json({
    //     bicicletas: Bicicleta.allBicis
    // })

    res.status(200);

    res.redirect('/bicicletas');
}

// Editar bicicleta : GET
exports.bicicleta_update_get = function (req, res) {
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', {
        bici
    });
}

// Editar bicicleta : POST
exports.bicicleta_update_post = function (req, res) {
    var bici = Bicicleta.findById(req.params.id);

    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}


exports.bicicleta_delete = function (req, res, next) {
    const id = req.body.id;
    Bicicleta.removeById(id);
    res.status(204);

    //Set HTTP method to GET
    req.method = 'GET'

    //redirect
    res.redirect('/bicicletas');

}