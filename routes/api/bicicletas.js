var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');


// LIST
router.get('/', bicicletaController.bicicleta_list);


// CREATE
router.post('/create', bicicletaController.bicicleta_create);


// UPDATE
router.get('/update/:id', bicicletaController.bicicleta_update_get);
router.post('/update/:id', bicicletaController.bicicleta_update_post);


//DELETE
/**
 * DELETE no funciona
 * intente con el paquete 'method-override' de express
 * y no encontré la solucion
 * 
 */
router.post('/delete', bicicletaController.bicicleta_delete);

module.exports = router;